
{{date} 09:58
Status: #done 
Tags: [[Linux]] [[Test Fix]]

# fix-linux
- UNIX vs linux
	- **UNIX:** A family of proprietary operating systems known for their stability and reliability.
	- **Linux:** An open-source operating system inspired by UNIX. It's free to use, modify, and distribute,

- what the '.' at the end of the file permission say
	- GNU `ls` uses a `.` character to indicate a file with an SELinux security context, but no other alternate access method.

- what are the file types in linux and how do you tell the, a apart
	- regular file, character device files, block device files, named pipes, sockets, symlink,  hidden files

- what's the difference between $HOME and '~' 

| feture    | $HOME                                 | ~                                               |
| --------- | ------------------------------------- | ----------------------------------------------- |
| Type      | Enviornment Variable                  | shell expansion symbol                          |
| Value     | Absolute path to home dir             | shorthand for your home dir                     |
| Usage     | Reference home dir path               | quickly access files/directorys                 |
| expension | Expands to full path before execution | Expands to `$HOME` (full path) before execution |

- how to delete a few lines upwards (vim)
	- d number k


- difference between wq and x
	- Use `wq` when you've finished editing a file and want to save your changes before exiting Vim.
	- use `x` only if you haven't made any changes to the file and want to exit quickly. 

- what type of terminals there are (focus on loacal and tty)
	- both local terminals and TTY terminals are pseudo-terminals offering a text-based interface for interacting with Linux.

- how to see all the open connections to the machine what's the command output says and  what is tty connections
	- netstat: This is a traditional tool for network monitoring and can display various information about network connections, including active connections, listening ports, and routing tables.
	- In the output of these commands, you might see connections with entries like "pts/0" (where 0 represents a number). These indicate TTY connections, which refer to pseudo-terminals. 

- in what path the files that represent remote connections  and what's the difference between that and tty
	- **Remote Connections:** Dynamic information maintained by the kernel, not individual files.
	- TTY Connections: Software emulations of terminals, not directly related to remote connections.

- what shells exist
	- sh, bash, Tcsh, ksh, zsh, fish

- what is tee
	- `tee` is a command-line utility used for duplicating the standard input (stdin) to both the standard output (stdout) and one or more files

- why cant you touch file[1-5]? why doesn't it create 5 files? why it works on delete
	- The shell interprets the expression `file[1-5]` before running the `touch` command. This expands to `file1 file2 file3 file4 file5`. So, `touch` receives five separate file names as arguments, not a pattern to create multiple files.
	- The `rm` (remove) command works differently because it typically interprets wildcards (`*`) within filenames for deletion. So, `rm file[1-5]` would expand to delete files named `file1` to `file5` if they exist. However, be cautious when using wildcards with `rm` as it can lead to accidental deletion of unintended files.

- what's the number in the man title mean
	- **Section 1:** User commands (programs you can execute from the shell). These are the most commonly used man pages, containing information about commands like `ls` (listing files), `cat` (displaying file contents), and `cp` (copying files).
	- **Section 2:** System calls (functions that provide an interface to the operating system kernel). These are typically used by programmers writing system programs and not directly accessed by users. Examples include `open` (opening a file) and `read` (reading data from a file).
	- **Section 3:** Library calls (functions within libraries used by programmers to build applications). These provide functionalities like `printf` (formatted output) and `strcpy` (copying strings).
	- **Section 4:** Special devices (files that represent hardware devices or resources). These provide access to devices like terminals (`tty`) and random number generators (`random`).
	- **Section 5:** File formats and conventions (information about the structure and contents of specific file types). Examples include system configuration files like `/etc/passwd` (user accounts) and cron jobs (`crontab`).
	- **Section 6:** Games and sports (less common section containing documentation for games). An example might be `man pacman` for the classic game Pacman (availability may vary).
	- **Section 7:** Miscellaneous (documentation for various topics that don't fit well into other categories). This might include things like `man ascii` (ASCII character codes).
	- **Section 8:** System administration (privileged commands used for system management tasks). These commands typically require root access and are used for tasks like mounting disks (`mount`) and configuring network interfaces (`ifconfig`).
	- **Section 9:** Kernel routines (functions used internally by the kernel). These are rarely used directly by users or even programmers and are for informational purposes.

- how man is structured
	- Title and Section, Name, Synopsis, Description, Options, Operands, Exit Status, Examples, See Also, Author, Bugs

- -f vs -k in man 
	- Use `-f` when you know the exact name of the item and just need a quick reminder of what it does.
	- Use `-k` when you're unsure of the exact name but have a general idea of the functionality you're looking for.

- what sort command does
	- The `sort` command in Linux is used to sort lines of text files in a specific order. It takes input from a file (or standard input) and arranges the lines based on the sort criteria you specify.

- why hidden files are hidden
	- to prevent overloading the user with files
	- for safety when the files are hidden the chances of accidentdal modfication is lower
- why when copying root file owner change as normal user when -p is used
	- because when the cp commaned is used without sudo the user doesnot have the premission to elevate the permission
- preserve permissions in mv 
	- there is no need to explicitly say to preserve it is done automaticly
- add file to locate db
	- sudo update db
- the name of the input streams number
	- file descriptors
- command substitution  in different ways
	- using backticks and $()
- pping vs different ways for command substitution
	- 404
- sbin/nologin and whats its shell
	- The `/sbin/nologin` program in Unix/Linux systems is used to prevent users from logging into the system. When a user attempts to log in with a shell set to `/sbin/nologin`, they are immediately logged out
- !* vs !! difference in shadow
	- both `!` and `!*` effectively prevent login using the account, but `!*` can be seen as a more explicit administrative notation.
- what is gshadow 
	- **`/etc/gshadow`**: Stores hashed group passwords and manages group access control with additional fields for group administrators and members.
- directory permssions meaning
	- **Read (`r`)**: Allows a user to list the contents of the directory.
	- **Write (`w`)**: Allows a user to create, delete, or rename files and subdirectories within the directory.
	- **Execute (`x`)**: Allows a user to enter (or traverse) the directory and access its contents. Without execute permission, even if a user can list the directory contents, they cannot access or modify the files within it.
- sticky bit meaning
	- **Sticky Bit**: A special permission that, when set on a directory, ensures that only the file owner, directory owner, or root user can delete or rename files within that directory.
- S in permissions
	- Indicates that the Setuid or Setgid permission is set, but the corresponding execute permission is not set. It means the special permission is active, but the execute bit for the owner or group is not set.
- how to make umask universal
	- Configure in `/etc/profile`, `/etc/bash.bashrc`, or `/etc/profile.d/`.
- first digit in umask meaning
	- Special permissions (Setuid, Setgid, Sticky Bit)
- umask file vs dir
	- **Files**: The `umask` subtracts from `666` to determine the default permissions. The `umask` affects read, write, and execute permissions based on the octal value specified.
	- **Directories**: The `umask` subtracts from `777` to determine the default permissions. The `umask` affects read, write, and execute permissions, but always ensures the execute bit is set for directories to allow them to be traversed.
- show disk data command
	- **`lsblk`**: Lists block devices and their attributes.
	- **`fdisk`**: Shows detailed partition information and requires root access.
- find port in use with lsof
	- lsof -i -n -P
- logs service
	- **`systemd-journald`**: Collects and manages logs, part of `systemd`.
	- **`rsyslog`**: Traditional and widely used syslog daemon.
	- **`syslog-ng`**: Alternative syslog daemon with advanced features.
- environment variable in different shell not found
	-  **Environment variables** are specific to the shell session and must be set according to the shell's syntax.
	- **Persistent variables** should be added to the shell's configuration files.
	- **System-wide variables** can be added to `/etc/environment` or `/etc/profile`.
- print shell variables
	- declare -p
- path variables location
	- - **`/etc/profile`**: Used by login shells for all users.
	- **`/etc/profile.d/`**: Directory containing scripts that are sourced by `/etc/profile`.
	- **`~/.bash_profile`** or **`~/.profile`**: For login shells in Bash. `~/.bash_profile` is specific to Bash, while `~/.profile` is more general and can be used by other shells as well.
- bash_history vs history
	- **`.bash_history`**: The file where Bash stores command history persistently across sessions.
	- **`history` Command**: Shows or manipulates the command history of the current shell session.
- set write to history
	- **`history -a`**: Appends the current session’s history to the history file.
	- **`PROMPT_COMMAND='history -a'`**: Automatically writes each command to the history file as soon as it is executed.
	- **Configuration Files**: Set these in `~/.bashrc` to ensure they are applied to all sessions.
- run order bash -> why bash_rc run again in a new shell
	- login shell(/etc/profile, ~/.bash_profile) -> none-login shell (bashrc)
	- When you open a new terminal or run a new instance of Bash from an existing terminal, a non-login shell is started. This non-login shell executes `~/.bashrc`.
- /dev/random function
	- **`/dev/random`**: Provides high-quality random data but may block if the entropy pool is insufficient.
- /bin vs /usr/bin
	- **`/bin`**: Contains essential system binaries required for basic operation and early boot phases. It is usually part of the root filesystem and must be available from the start.
	- **`/usr/bin`**: Contains non-essential user binaries and applications. It may be on a separate partition and mounted later in the boot process.
- what is /tmp
	- **`/tmp`**: A standard directory for temporary file storage in Unix-like systems, used by applications for transient files and inter-process communication.
- what object contains file meta data
	- file metadata is maintained in an object known as the **inode**. The inode (index node) contains various pieces of information about a file or directory, but it does not include the file name or its data. Instead, it stores the metadata necessary for managing the file system.
- what is lost and found lost+found 
	- **`lost+found`**: A special directory in Unix-like filesystems used to recover files that have been lost or corrupted due to filesystem errors.
- how files get to lost and found
	- `fsck` scans and repairs filesystem errors, placing orphaned or damaged files in `lost+found` for manual inspection and recovery.
- what syscall comes after fork
	- after the `fork()` system call in Unix-like operating systems, the next typical step is often to call `exec()` or one of its variants
- what is the name of the place where the process points to (copy on write)
	- Before modification, processes created by `fork()` point to the same shared memory pages.
- exec ls why closes shell
	- The shell process is replaced by the `ls` command. Once `ls` finishes executing, there is no shell process to return to, so the terminal session ends.
- process vs deamon
	-  **Process**: A general term for a running program, which can be interactive or background.
	- **Daemon**: A specific type of process that runs in the background, performs system-level tasks, and does not interact directly with users.
- process when does is it done and zombie process
	- The terminating process provides an exit status code to the OS, which indicates the result of the process's execution. This exit status is used to determine if the process completed successfully or encountered errors.
	- Created when a process terminates, but its parent has not yet collected the exit status. The terminated process remains in the process table as a zombie until the parent calls `wait()`.
- move proc to bg
	- add & at the end and it will start at the background
	- ctrl + z will move a process to the background
- how a proc gets signals
	- Use `signal()` or `sigaction()` to set up handlers, `kill` or system calls to send signals, and `sigprocmask()` to block signals.
- list 4 types of signal
	- **Signal Interrupt (`SIGINT`)**: User interruption (Ctrl+C).
	- **Signal Terminate (`SIGTERM`)**: Request to terminate the process.
	- **Signal Kill (`SIGKILL`)**: Immediate process termination.
	- **Signal Hangup (`SIGHUP`)**: Terminal disconnection or reload configuration.
- what happens in the fork bomb command
	- **`:()`**: Defines a function named `:` (colon).
	- **`{ :|:& };`**: The function body. Inside the function:
	    - `:|:`: The function calls itself twice, creating two new processes.
	    - `&`: The processes are run in the background.
	- **`;`**: Ends the function definition.
	- **`:`**: Executes the function, causing it to start creating new processes.
- top command fields
    - user: shows the username of the process owner
    - PID: show the process id the assigned to each process
    - PR: priority of the process. the lower the number the higher the priority
    - NI: stands for "Nice" value, which allows you to to adjust a process priority slightly
    - VIRT: shows the virtual memory size used by the process
    - RES: the amount of physical memory the process is using
    - SHR: idicates the amount of memory the process shares with other proces's
    - S: this represents the process status
    - %CPU: shows the precentage of the cpu time the process has used since the last top update
- command to load config in systemctl
    -?
- where the output goes to and how to show it on screen
    - by default the output goes to the mail inbox
    -to show the out put you can use command | tee /dev/stdout
- what was in use before ssh
    - rlogin, rsh, serial console access
- state full vs stateless
    - statefull remember past interactions, Reli on storage, more complex to manage 
- ssh algorithm how it work
    - df algorithm uses mathemathical operations on large prime numbers ecdh levrages the propreties ot the elliptic curves which are a mathemathical structure defined by a specifec equation 
    - elliptic curves offer advantage like smaller key sizes compared to traditional dh this translates to faster computations and less resource usage on the device
- steps in ssh
    - client initialization
    - server response
    - key exchange 
    - authentication
    - encription and secure communication
- authorized keys 
    - an authorized key consists of a key pair
        - Public key: this key is stored on the server you want to access. its freelyy distributable as it cannot ne used to devrypt data
        - Private key: this key is kept secrete on your local machine and should never be shared



References:
