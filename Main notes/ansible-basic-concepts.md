
2024-06-19 19:22
Status: #inProgress 
Tags: [[DevOps]] [[RedHat]] [[Ansible]]

# ansible
## Understanding ansible concepts

### [Control node](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id1)

The machine from which you run the Ansible CLI tools (ansible-playbook , ansible, ansible-vault and others). You can use any computer that meets the software requirements as a control node - laptops, shared desktops, and servers can all run Ansible. You can also run Ansible in containers known as [Execution Environments](https://ansible.readthedocs.io/en/latest/getting_started_ee/index.html).

Multiple control nodes are possible, but Ansible itself does not coordinate across them

  

### [Managed nodes](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id2)

Also referred to as ‘hosts’, these are the target devices (servers, network appliances or any computer) you aim to manage with Ansible.

Ansible is not normally installed on managed nodes, unless you are using ansible-pull, but this is rare and not the recommended setup.

  

### [Inventory](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id3)

A list of managed nodes provided by one or more ‘inventory sources’. Your inventory can specify information specific to each node, like IP address. It is also used for assigning groups, that both allow for node selection in the Play and bulk variable assignment.

  

### [Playbooks](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id4)

They contain Plays (which are the basic unit of Ansible execution). This is both an ‘execution concept’ and how we describe the files on which ansible-playbook operates.

Playbooks are written in YAML and are easy to read, write, share and understand.

  

#### [Plays](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id5)

The main context for Ansible execution, this playbook object maps managed nodes (hosts) to tasks. The Play contains variables, roles and an ordered lists of tasks and can be run repeatedly. It basically consists of an implicit loop over the mapped hosts and tasks and defines how to iterate over them

##### [Roles](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id6)

A limited distribution of reusable Ansible content (tasks, handlers, variables, plugins, templates and files) for use inside of a Play.

To use any Role resource, the Role itself must be imported into the Play.

##### [Tasks](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id7)

The definition of an ‘action’ to be applied to the managed host. You can execute a single task once with an ad hoc command using ansible or ansible-console (both create a virtual Play).

##### [Handlers](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id8)

A special form of a Task, that only executes when notified by a previous task which resulted in a ‘changed’ status.

  

### [Modules](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id9)

The code or binaries that Ansible copies to and executes on each managed node (when needed) to accomplish the action defined in each Task.

Each module has a particular use, from administering users on a specific type of database to managing VLAN interfaces on a specific type of network device.

You can invoke a single module with a task, or invoke several different modules in a playbook. Ansible modules are grouped in collections. For an idea of how many collections Ansible includes, see the [Collection Index](https://docs.ansible.com/ansible/latest/collections/index.html#list-of-collections).

### [Plugins](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id10)

Pieces of code that expand Ansible’s core capabilities. Plugins can control how you connect to a managed node (connection plugins), manipulate data (filter plugins) and even control what is displayed in the console (callback plugins).

### [Collections](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#id11)

A format in which Ansible content is distributed that can contain playbooks, roles, modules, and plugins. You can install and use collections through [Ansible Galaxy](https://galaxy.ansible.com).

## Questions
What are the differences between Ansible and other tools like Chef or Puppet?

Here's a breakdown of the key differences between Ansible and other configuration management tools like Chef and Puppet:

  

**Agent vs. Agentless:**

  

* **Ansible:** Agentless - No pre-installed software required on managed nodes. It connects and executes tasks using SSH.

* **Chef & Puppet:** Client-Server Architecture - Require a client agent to be installed on managed nodes for communication with the server.

  

**Configuration Language:**

  

* **Ansible:** YAML - Human-readable format for defining configurations.

* **Chef:** DSL (Domain-Specific Language) - Requires learning a custom language for writing configurations.

* **Puppet:** Puppet DSL - Another custom language specifically for Puppet.

  

**Idempotence:**

  

* All three tools strive for idempotence, meaning they guarantee the desired state is achieved regardless of how many times you run them.

  

**Additional Considerations:**

  

* **Flexibility:** Ansible is often considered more flexible due to its simpler configuration language and agentless approach.

* **Security:**  While all three can be secure, some may require additional configuration for agent communication (Chef/Puppet).

* **Community:** Ansible has a large and active community, which can be helpful for finding support and resources.

  
  

In essence, Ansible offers a simpler and more lightweight approach compared to Chef and Puppet, making it easier to learn and use for basic configuration management tasks. However, Chef and Puppet might be preferable for very complex infrastructures or those requiring stricter control over agent communication.

  

The `when` option in Ansible playbooks allows you to conditionally execute tasks based on a boolean expression. This provides a powerful way to control which tasks run depending on the specific state of the target system.

  

**How to Use It:**

  

What is the when option and how do you use it?

The `when` option is specified within a task definition and takes a boolean expression as its argument. This expression can involve:

  

* **Variables:** Defined within the playbook or retrieved from system facts.

* **Facts:** Information gathered about the system during playbook execution (e.g., OS version, uptime).

* **Jinja2 templating:** For more complex logic using templating filters and conditionals within the expression.

  

**Example:**

  

```yaml

- name: Reboot if load average is high

  reboot:

  when: uptime |  float  >= 5.0

```

  

In this example:

  

* The task name is "Reboot if load average is high".

* The `reboot` module is used to reboot the system.

* The `when` option is set with a boolean expression.

* `uptime | float` uses a pipe filter to convert the uptime to a floating-point number.

* `>= 5.0` checks if the uptime is greater than or equal to 5 minutes.

  

This task will only reboot the system if the uptime condition is met.

  

**Benefits:**

  

* **Conditional Execution:** Ensures tasks are only run when necessary, making playbooks more efficient and targeted.

* **Flexibility:**  You can create complex conditional logic using variables, facts, and Jinja2 templating.

* **Modular Playbooks:**  Break down playbooks into smaller, well-defined tasks with conditional execution for better organization and maintainability.

  
  
  
  
  
  
  

How do you extend a conditional over multiple tasks without defining the when conditional multiple times?:

Ansible provides a couple of ways to extend a conditional over multiple tasks without repeatedly defining the `when` option for each task. Here are two common approaches:

  

**1. Using Blocks:**

  

Blocks allow you to group a set of tasks and apply a conditional logic block to the entire group. This is a cleaner and more concise way to handle conditional execution for multiple tasks.

  

**Example:**

  

```yaml

- name: Check high load

  block:

when: uptime | float >= 5.0  \#Define the condition here

tasks:

   - name: Reboot if load is high

     reboot:

   - name: Notify admins

     mail:

       to: admins@example.com

       subject: Server Load High

```

  

Here, the `when` condition is defined within the block. All tasks nested within the block will only be executed if the condition evaluates to true.

  

**2. Using Include/Import with Conditional Playbook:**

  

Another approach is to create a separate playbook containing the tasks you want to conditionally execute. Then, use `include` or `import_tasks` within your main playbook, along with a conditional statement to control its inclusion.

  

**Example:**

  

1. Create a separate playbook named `high_load_actions.yml`:

  

```yaml

- name: Reboot if load is high

  reboot:

- name: Notify admins

  mail:

to: admins@example.com

subject: Server Load High

```

  

2. In your main playbook, use `include` with a `when` condition:

  

```yaml

- include: high_load_actions.yml

  when: uptime | float >= 5.0

```

  

This approach keeps your main playbook cleaner and separates reusable tasks. The `high_load_actions.yml` playbook will only be included and executed if the `when` condition is true.

  

**Choosing the Right Approach:**

  

* **Blocks:** Ideal for situations where the conditional logic applies to a small group of closely related tasks within the same playbook.

* **Include/Import:** Useful when you have reusable tasks with conditional execution that can be used across different playbooks. It promotes better code organization and modularity.

  

Q: What is the diffrence between include and import?

Ans: Both `include` and `import` directives in Ansible allow you to incorporate tasks from external YAML files into your main playbook. However, there's a subtle difference in how they handle variable and namespace management:

  

**Include:**

  

* **Simple Insertion:** Inserts the tasks from the included file at the current location within your main playbook.

* **Variable Scope:** Variables defined in the included file share the same namespace as the main playbook. This means any variable names defined in both files must be unique to avoid conflicts.

* **Updates Reflected:** Any changes made to the included YAML file are reflected when you run the main playbook again.

  

**Import:**

  

* **Namespaced Tasks:** Tasks from the imported file are imported with a namespace prefix corresponding to the filename (minus the `.yml` extension). This helps prevent naming conflicts with tasks in the main playbook.

* **Separate Namespace:** Variables defined in the imported file reside in a separate namespace from the main playbook. This avoids accidental overrides or conflicts.

* **Static Import:** The tasks are imported at the beginning of the playbook execution, and any changes to the imported YAML file won't be reflected during the current run.

  

**Choosing Between Include and Import:**

  

* Use `include` when you have a small set of closely related tasks and want them to share the same variable namespace as the main playbook.

* Use `import` when you want to import reusable tasks with their own namespace to avoid conflicts. This is especially beneficial for modularizing playbooks and using the same tasks across multiple playbooks.

* If you need to dynamically update tasks based on changes in the included file during a single playbook run, consider using `include_tasks` instead of `include`. `include_tasks` re-reads the included file at the point of execution, reflecting any modifications.

  

Here's a table summarizing the key differences:

  

| Feature             | Include             | Import             |

|--------------------------|-------------------------|-------------------------|

| Task Insertion       | Inserts at current point | Imports at beginning |

| Variable Scope      | Shared namespace    | Separate namespace  |

| Update Reflection    | Reflected in current run | Not reflected (static)  |

| Namespace for Tasks | None                | Filename prefix     |

| Use Case             | Simple, related tasks   | Reusable, modular tasks |

  
  

Q: add cmd args

Ans: There are two main ways to pass variables at the command line when running Ansible playbooks:

  

**1. Using the `-e` option:**

  

The `-e` option (short for extra-vars) allows you to define key-value pairs for variables that you want to make available within your playbook. Here's the syntax:

  

```

ansible-playbook <playbook.yml> -e <variable_name>=<variable_value>

```

  

**Example:**

  

```

ansible-playbook install_apache.yml -e target_server=webserver1 package_name=httpd

```

  

In this example:

  

* `install_apache.yml` is the playbook you want to run.

* `target_server` is a variable set to "webserver1". This variable can be accessed within the playbook using Jinja2 templating syntax `{{ target_server }}`.

* `package_name` is another variable set to "httpd". This allows you to dynamically specify the package name within the playbook tasks.

  

**2. Using a Variable File:**

  

You can create a YAML file containing variables and pass it to the `ansible-playbook` command using the `-e` option along with the `@` symbol:

  

```yaml

\# variables.yml

target_server: webserver1

package_name: httpd

  

\# Running the playbook

ansible-playbook install_apache.yml -e @variables.yml

```

  

This approach helps keep your variables organized in a separate file and promotes code reusability.

  

**Additional Notes:**

  

* You can pass multiple variables at the same time using the `-e` option with multiple key-value pairs.

* Values passed at the command line using `-e` have higher precedence than variables defined within the playbook itself. This allows you to override default values defined in the playbook for specific runs.

* Be cautious with sensitive information like passwords when passing variables at the command line. Consider using alternative methods like environment variables or Ansible Vault for storing secrets.

  

Q: ask for prompts

Ans: Ansible provides the prompt module to request user input during playbook execution. This allows you to capture dynamic information from the user and use it within your tasks.

  

Q: why committing secrete data bad

Ans: Committing secret data like passwords, API keys, or encryption keys into a Git repository is a serious security risk for several reasons:

  

* **Version Control Exposure:**  Git is a version control system, meaning all revisions of your code, including past commits, are stored and accessible. Even if you remove the secret from a later commit, someone with access to the repository could potentially access it from an older version.

* **Accidental Leaks:** Accidental commits can happen due to human error. Pushing a commit with sensitive data before realizing it can expose your secrets.

* **Broader Repository Access:** Repositories might be shared with a wider group of people than you initially intended. Collaborators, testers, or even external parties might accidentally or intentionally gain access to sensitive information.

* **Compromised Credentials:** If an attacker gains access to your Git repository credentials, they have full access to all secrets stored within it.

  

Q: What are some of the ways to keep secret data out of a repository (Using Ansible)?

Ans: Ansible Vault is a built-in feature that allows you to encrypt sensitive information within your Ansible playbooks, variable files, or inventory files. This protects your secrets, such as passwords, API keys, or encryption keys, from being exposed in your version control system (VCS) like Git.

  

**Here's how Ansible Vault works:**

  

1. **Encryption:** You define a secret password for Ansible Vault. This password is used to encrypt the specific data you want to protect within your files. The encrypted data appears as gibberish within the files stored in your VCS repository, making it unreadable to anyone without the decryption password.

2. **Decryption on-the-fly:** During playbook execution, Ansible Vault utilizes the password you provided to decrypt the data on-demand for use within your tasks. This ensures that the sensitive information is only revealed when necessary and within the controlled environment of your Ansible automation.

  

**Benefits of Using Ansible Vault:**

  

* **Improved Security:** By encrypting secrets, you significantly reduce the risk of unauthorized access if your VCS repository is compromised.

* **Seamless Integration:** Ansible Vault is a built-in feature, so it integrates seamlessly with Ansible. There's minimal additional configuration required to start using it.

* **Ease of Use:** Ansible Vault offers a relatively simple way to encrypt and decrypt data within your playbooks.

  
  

**Drawbacks and Considerations:**

  

* **Single Password:** All your secrets are encrypted with the same password. If this password is compromised, all your secrets become vulnerable.

* **Password Management:** You need to manage and securely store the decryption password. A compromise of this password would negate the security benefits of Ansible Vault.

* **Limited Access Control:** Ansible Vault doesn't offer fine-grained access control for your secrets. Everyone with the decryption password has access to all encrypted data.

  
  

**When to Use Ansible Vault:**

  

* Ansible Vault is a good option for simple use cases where you have a few secrets to protect and don't require advanced access control features.

* It's a convenient way to get started with securing secrets within your Ansible playbooks.

  
  

**Alternatives for Stronger Security:**

  

For production environments with stricter security requirements, consider using external secrets management tools like HashiCorp Vault, AWS Secrets Manager, or Azure Key Vault. These tools offer features like:

  

* **Centralized Storage:** Secrets are stored outside your VCS repository, adding an extra layer of security.

* **Fine-grained Access Control:** You can define granular permissions for who can access and use specific secrets.

* **Auditing and Logging:** These tools provide detailed logs and audit trails to track access attempts and usage of secrets.

  

**Choosing the Right Approach:**

  

The choice between Ansible Vault and external secrets management tools depends on your specific security needs and the complexity of your environment.

  

* For simple use cases with a few secrets, Ansible Vault can be a good starting point.

* For production environments with stricter security requirements and a larger number of secrets, external secrets management tools offer a more robust and scalable solution.

  

Q: Where would you save Ansible configurations?

Ans: There are several common locations to store Ansible configurations, depending on their scope and purpose:

  

**1. Inventory File:**

  

* Typically stored in a central location accessible to all Ansible control nodes.

* Common locations include:

* `/etc/ansible/hosts` (default location on many Linux distributions)

* A shared directory accessible by all control nodes (e.g., `/shared/ansible/inventory`)

  

**2. Playbooks:**

  

* Playbooks can be stored in a central version control system (VCS) like Git for collaboration and version control.

* A common directory structure might be:

* `/playbooks` (generic location)

* Project-specific directories within `/playbooks` (e.g., `/playbooks/webservers`, `/playbooks/databases`)

  

**3. Roles:**

  

* Ansible roles, which are reusable collections of tasks and variables, are often stored in a VCS.

* Common locations include:

* A dedicated role repository like Ansible Galaxy

* A project-specific roles directory within your VCS (e.g., `/roles`)

  

**4. Variable Files:**

  

* Variable files, containing key-value pairs used within playbooks, can be stored in various locations:

* **Within Playbooks:** Define variables directly within your playbook YAML file.

* **Separate YAML Files:** Store variables in separate YAML files for better organization and reusability. These files can be located in the same directory as your playbooks or a centralized location.

* **Grouped by Environment:** If you have different environments (e.g., dev, test, prod), consider separate variable files for each environment to manage configuration differences.

  

**5. Ansible Configuration File (ansible.cfg):**

  

* The primary Ansible configuration file, `ansible.cfg`, is typically located in:

* `/etc/ansible/ansible.cfg` (default location)

* You can override this by setting the `ANSIBLE_CONFIG` environment variable or specifying a path in your command-line arguments.

  

**Choosing the Right Location:**

  

The ideal storage location depends on the specific configuration element and your overall infrastructure setup. Here are some general guidelines:

  

* **Centralized Access:** Inventory files and configuration files (`ansible.cfg`) should be accessible by all Ansible control nodes for consistent execution.

* **Version Control:** Use a VCS like Git to manage playbooks, roles, and reusable variable files for collaboration, version history, and rollback capabilities.

* **Separation of Concerns:** Separate sensitive information like passwords from playbooks and store them securely using Ansible Vault or external secrets management tools.

  

By following these practices, you can organize your Ansible configurations effectively, promoting maintainability, collaboration, and security within your automation workflows.

  

Q: where to read on ansible-modules

Ans: There are two primary places to look for documentation about an Ansible module:

  

1. **Ansible Project Documentation:**

  

   * The official Ansible documentation website provides comprehensive information about modules, plugins, concepts, and best practices.

   * The specific location for module documentation depends on the Ansible version you're using. Here's how to find it:

  

    * **Search the online documentation:**

        * Go to the Ansible documentation website: [https://docs.ansible.com/](https://docs.ansible.com/).

        * In the search bar, enter the name of the module you're interested in (e.g., "firewalld").

        * The search results should include the relevant module documentation page.

  

    * **Browse the documentation by version:**

        * Navigate to the Ansible documentation website.

        * Select the "Docs" tab and choose your Ansible version from the dropdown menu.

        * Within the version-specific documentation, explore sections like "Library Reference" or "Modules" to find the specific module you need.

  

2. **Module Documentation within Ansible:**

  

   * Ansible also provides a way to access module documentation directly from the command line. This can be helpful when you're already working in your Ansible environment.

   * Use the `ansible-module-docs` command followed by the module name:

  

    ```bash

    ansible-module-docs <module_name>

    ```

  

    * This command displays the module documentation directly in your terminal.

  

**Additional Resources:**

  

* **Ansible Galaxy:**

* If the module you're interested in comes from a community-developed role available on Ansible Galaxy, the role documentation might also provide specific information about the module's usage within that role.

  

**Choosing the Right Source:**

  

* The official Ansible documentation website is the most reliable and up-to-date source for module documentation.

* The `ansible-module-docs` command is a convenient way to access basic documentation within your Ansible environment.

* Consider checking Ansible Galaxy role documentation if the module belongs to a specific role you're using.

  

By using these resources effectively, you can find the information you need to understand and utilize Ansible modules for your automation tasks.

Q:install yum package in ansible

Ans: he yum module is the primary way to manage packages with the yum package manager in Ansible. Here's the basic syntax:

YAML

- name: Install package

  yum:

    name: <package_name>

    state: present  # Ensures the package is installed (default)

Q: How does Ansible indicate if something changed?

Ans: Ansible indicates if something changed during a task execution by returning a specific status. There are two main ways to determine if a change occurred:

  

**1. Task Status:**

  

Ansible playbooks are composed of individual tasks. Each task returns a status after execution, which can be:

  

* **changed:** This indicates that the task successfully made a change to the target system's configuration or state. This is the desired outcome for most configuration management tasks.

* **ok:** This status signifies that the task ran successfully, but no change was necessary. This might occur if the desired configuration already exists on the target system.

* **failed:** This status means the task encountered an error and failed to complete successfully. You'll need to investigate the error output for troubleshooting.

* **skipped:** This status indicates the task was skipped intentionally, either due to conditional logic within the playbook or missing requirements on the target system.

  

**2. Playbook Summary:**

  

After executing a playbook, Ansible displays a summary report. This report outlines the status (changed, ok, failed, skipped) for each task executed within the playbook. Here's what to look for:

  

* **CHANGED (X):** This signifies that at least one task within the playbook resulted in a change.

* **OK:** This indicates that all tasks ran successfully, but none of them made any changes (possibly because the desired configuration already existed).

* **FAILED (F):** This means at least one task failed during execution. You'll need to review the playbook output for specific error messages.

  

**Understanding the Difference:**

  

* **Task-Level Change:** The "changed" status at the task level indicates that a specific action within the task modified the target system.

* **Playbook-Level Change:** The "CHANGED (X)" in the playbook summary signifies that at least one task within the entire playbook resulted in a change.

  

Here are some additional points to consider:

  

* **Idempotence:** Ansible strives for idempotence, meaning a task should produce the same outcome (changed or ok) on subsequent runs if the target system's state hasn't changed manually.

* **Verbosity:**  You can control the level of detail displayed in the playbook summary using the `-v` or `--verbose` option with the `ansible-playbook` command. Higher verbosity levels provide more information about task execution and changes.

  

By understanding these concepts, you can effectively interpret Ansible's status outputs to determine if your playbooks are successfully making the desired changes to your managed systems.

  

Q: Where are some places variables can be registered?

Ans: In Ansible, variables can be registered during task execution using the `register` keyword. Here are the common places where you can register variables:

  

1. **Within Tasks:**

  

This is the most common scenario. You can register the output of any module within a task using the `register` keyword. The output is then stored in a variable with a user-defined name for later use:

  

```yaml

- name: Get system uptime

  debug:

msg: "System uptime: {{ uptime }} seconds"

  register: uptime_info

```

  

In this example:

  

* The `debug` module displays the system uptime.

* The `register: uptime_info` part captures the output (uptime in seconds) and stores it in a variable named `uptime_info`.

  

2. **Inside Loops:**

  

When using loops in Ansible playbooks, you can register the results for each iteration:

  

```yaml

- name: Manage user accounts

  user:

name: "{{ item.username }}"

state: "{{ item.state }}"

  loop: "{{ users }}"

  register: user_results

```

  

Here:

  

* The loop iterates over a list of user definitions (`users`).

* The `register: user_results` captures the results (success/failure) for each user creation/deletion in a variable named `user_results`.

  

3. **Within Blocks:**

  

Blocks allow you to group tasks with conditional logic. You can register the outcome of the entire block execution:

  

```yaml

- name: Reboot if uptime exceeds threshold

  block:

when: uptime | float >= uptime_threshold

tasks:

   - name: Reboot the server

     reboot:

  register: reboot_action

```

  

* This block checks the uptime and reboots only if it exceeds the threshold.

* `register: reboot_action` captures whether a reboot occurred (changed) or not (ok).

  

4. **For Handlers:**

  

Handlers are tasks that execute only when a specific condition is met. You can register information about the triggering event within the handler:

  

```yaml

- name: Notify admins on reboot

  mail:

to: admins@example.com

subject: "Server {{ ansible_hostname }} Rebooted"

  when: changed  # Run only if a reboot occurred (from previous task)

  register: mail_sent

```

  

* This handler sends an email notification on reboot.

* `register: mail_sent` captures whether the email was sent (changed) or not (failed).

  

By understanding these locations for registering variables, you can effectively capture and utilize the output of tasks and blocks within your Ansible playbooks for conditional execution, data manipulation, and reporting purposes.

  

Q: How are variables referenced?

Ans: In Ansible, variables are referenced within playbooks, tasks, and templates using a specific syntax. Here's how you can use variables effectively:

  

**1. Double Curly Braces (Jinja2 Templating):**

  

The primary method for referencing variables is through double curly braces `{{ }}`. This syntax leverages Jinja2 templating, which allows you to embed variables within strings and data structures.

  

```yaml

- name: Print system information

  debug:

msg: "Hostname: {{ ansible_hostname }}, Uptime: {{ uptime }} seconds"

```

  

In this example:

  

* `ansible_hostname` is a fact (predefined variable) containing the hostname of the managed system.

* `uptime` is likely a variable registered in a previous task to store the system uptime.

  

**2. Accessing Dictionary Values:**

  

You can access specific values within dictionaries using dot notation:

  

```yaml

- name: Configure service

  systemd:

name: my_service

state: started

enabled: yes

daemon_reload: yes

  vars:

service_config:

   Restart: "on-failure"

   User: "myuser"

```

  

Here:

  

* The `service_config` dictionary defines service configuration options.

* Within the `systemd` module, individual options like `Restart` and `User` reference their corresponding values from the dictionary using dot notation (`service_config.Restart`, `service_config.User`).

  

**3. Loops and Conditionals:**

  

Variables can be used within loops and conditional statements to control playbook execution:

  

```yaml

- name: Manage users

  user:

name: "{{ item.username }}"

state: "{{ item.state }}"

  loop: "{{ users }}"

  when: item.state == "present"  # Only create users if state is "present"

```

  

* This example iterates through a list of users (`users`) defined as a variable.

* The loop references individual user information within the `user` module using `item.username` and `item.state`.

* The `when` condition ensures user creation only occurs if the `state` is set to "present".

  

**4. Filters (Optional):**

  

Jinja2 offers various filters you can apply to manipulate variable values within templates:

  

```yaml

- name: Display formatted uptime

  debug:

msg: "Uptime: {{ uptime | float | round(2) }} seconds"

```

  

* This example uses filters:

* `float`: Converts uptime to a floating-point number.

* `round(2)`: Rounds the uptime value to two decimal places.

  

**5. Referencing Facts:**

  

Ansible provides predefined variables called facts, which contain information about the managed systems. These facts can be directly referenced using the `ansible_` prefix:

  

```yaml

- name: Check if package is installed

  debug:

msg: "Package httpd is installed: {{ ansible_pkgs.contains('httpd') }}"

```

  

* This example checks if the `httpd` package is installed using the `ansible_pkgs.contains` fact.

  

**Remember:**

  

* Ensure the variable you're referencing is defined or registered before attempting to use it.

* Use appropriate quoting (single or double quotes) when referencing variables within strings.

  

By following these guidelines, you can effectively reference variables to create dynamic and data-driven playbooks in Ansible.

  

Q: How do you loop over a list or a dictionary in Ansible?

Ans:Ansible offers two primary ways to loop over a list or dictionary within your playbooks:

  

**1. Looping over a List:**

  

The `with_items` keyword is the most common method for iterating over a list of items in Ansible. Here's the syntax:

  

```yaml

- name: Loop over a list of users

  user:

name: "{{ item }}"

state: present

  with_items: "{{ users }}"

```

  

**Explanation:**

  

* `- name`: Defines the task name.

* `user`: Specifies the module you want to execute for each item in the loop (creates users in this example).

* `with_items`: This keyword initiates the loop.

* `{{ users }}`: This references the variable containing the list of items (usernames in this case).

* `{{ item }}`: Inside the loop, `item` represents the current item during each iteration. You can access its value within the module arguments.

  

**2. Looping over a Dictionary:**

  

To iterate over key-value pairs within a dictionary, use the `loop_control` keyword:

  

```yaml

- name: Configure services

  systemd:

name: "{{ item.key }}"

state: started

enabled: yes

  loop_control:

loop_var: item

  loop: "{{ services }}"

```

  

**Explanation:**

  

* `loop_control`: This keyword initiates the loop for dictionaries.

* `loop_var: item`: This defines the variable name (`item` in this example) that represents each key-value pair during iteration.

* `loop: "{{ services }}`: This references the dictionary variable containing service configurations (key: service name, value: configuration options).

* Inside the loop, you can access the key using `item.key` and the value using `item.value` (depending on your dictionary structure).

  

**Additional Considerations:**

  

* **Loop Filters (Optional):** You can use Jinja2 filters within loops to manipulate items before using them in modules.

* **Conditionals within Loops:** You can combine loops with conditional statements (`when`) to control task execution based on specific item properties.

* **Looping over Nested Structures:** Ansible allows looping over nested lists or dictionaries within loops for more complex scenarios.

  

Understanding these methods allows you to automate repetitive tasks for multiple systems or resources based on the data provided in your lists or dictionaries.

  

By effectively using loops in your Ansible playbooks, you can achieve efficient and scalable automation for managing your infrastructure.


What are the differences between Ansible and other tools like Chef or Puppet?:

Here's a breakdown of the key differences between Ansible and other configuration management tools like Chef and Puppet:

  

**Agent vs. Agentless:**

  

* **Ansible:** Agentless - No pre-installed software required on managed nodes. It connects and executes tasks using SSH.

* **Chef & Puppet:** Client-Server Architecture - Require a client agent to be installed on managed nodes for communication with the server.

  

**Configuration Language:**

  

* **Ansible:** YAML - Human-readable format for defining configurations.

* **Chef:** DSL (Domain-Specific Language) - Requires learning a custom language for writing configurations.

* **Puppet:** Puppet DSL - Another custom language specifically for Puppet.

  

**Idempotence:**

  

* All three tools strive for idempotence, meaning they guarantee the desired state is achieved regardless of how many times you run them.

  

**Additional Considerations:**

  

* **Flexibility:** Ansible is often considered more flexible due to its simpler configuration language and agentless approach.

* **Security:**  While all three can be secure, some may require additional configuration for agent communication (Chef/Puppet).

* **Community:** Ansible has a large and active community, which can be helpful for finding support and resources.

  
  

In essence, Ansible offers a simpler and more lightweight approach compared to Chef and Puppet, making it easier to learn and use for basic configuration management tasks. However, Chef and Puppet might be preferable for very complex infrastructures or those requiring stricter control over agent communication.

  

The `when` option in Ansible playbooks allows you to conditionally execute tasks based on a boolean expression. This provides a powerful way to control which tasks run depending on the specific state of the target system.

  

**How to Use It:**

  

What is the when option and how do you use it?

The `when` option is specified within a task definition and takes a boolean expression as its argument. This expression can involve:

  

* **Variables:** Defined within the playbook or retrieved from system facts.

* **Facts:** Information gathered about the system during playbook execution (e.g., OS version, uptime).

* **Jinja2 templating:** For more complex logic using templating filters and conditionals within the expression.

  

**Example:**

  

```yaml

- name: Reboot if load average is high

  reboot:

  when: uptime |  float  >= 5.0

```

  

In this example:

  

* The task name is "Reboot if load average is high".

* The `reboot` module is used to reboot the system.

* The `when` option is set with a boolean expression.

* `uptime | float` uses a pipe filter to convert the uptime to a floating-point number.

* `>= 5.0` checks if the uptime is greater than or equal to 5 minutes.

  

This task will only reboot the system if the uptime condition is met.

  

**Benefits:**

  

* **Conditional Execution:** Ensures tasks are only run when necessary, making playbooks more efficient and targeted.

* **Flexibility:**  You can create complex conditional logic using variables, facts, and Jinja2 templating.

* **Modular Playbooks:**  Break down playbooks into smaller, well-defined tasks with conditional execution for better organization and maintainability.

  
  
  
  
  
  
  

How do you extend a conditional over multiple tasks without defining the when conditional multiple times?:

Ansible provides a couple of ways to extend a conditional over multiple tasks without repeatedly defining the `when` option for each task. Here are two common approaches:

  

**1. Using Blocks:**

  

Blocks allow you to group a set of tasks and apply a conditional logic block to the entire group. This is a cleaner and more concise way to handle conditional execution for multiple tasks.

  

**Example:**

  

```yaml

- name: Check high load

  block:

when: uptime | float >= 5.0  # Define the condition here

tasks:

   - name: Reboot if load is high

     reboot:

   - name: Notify admins

     mail:

       to: admins@example.com

       subject: Server Load High

```

  

Here, the `when` condition is defined within the block. All tasks nested within the block will only be executed if the condition evaluates to true.

  

**2. Using Include/Import with Conditional Playbook:**

  

Another approach is to create a separate playbook containing the tasks you want to conditionally execute. Then, use `include` or `import_tasks` within your main playbook, along with a conditional statement to control its inclusion.

  

**Example:**

  

1. Create a separate playbook named `high_load_actions.yml`:

  

```yaml

- name: Reboot if load is high

  reboot:

- name: Notify admins

  mail:

to: admins@example.com

subject: Server Load High

```

  

2. In your main playbook, use `include` with a `when` condition:

  

```yaml

- include: high_load_actions.yml

  when: uptime | float >= 5.0

```

  

This approach keeps your main playbook cleaner and separates reusable tasks. The `high_load_actions.yml` playbook will only be included and executed if the `when` condition is true.

  

**Choosing the Right Approach:**

  

* **Blocks:** Ideal for situations where the conditional logic applies to a small group of closely related tasks within the same playbook.

* **Include/Import:** Useful when you have reusable tasks with conditional execution that can be used across different playbooks. It promotes better code organization and modularity.

  

Q: What is the diffrence between include and import?

Ans: Both `include` and `import` directives in Ansible allow you to incorporate tasks from external YAML files into your main playbook. However, there's a subtle difference in how they handle variable and namespace management:

  

**Include:**

  

* **Simple Insertion:** Inserts the tasks from the included file at the current location within your main playbook.

* **Variable Scope:** Variables defined in the included file share the same namespace as the main playbook. This means any variable names defined in both files must be unique to avoid conflicts.

* **Updates Reflected:** Any changes made to the included YAML file are reflected when you run the main playbook again.

  

**Import:**

  

* **Namespaced Tasks:** Tasks from the imported file are imported with a namespace prefix corresponding to the filename (minus the `.yml` extension). This helps prevent naming conflicts with tasks in the main playbook.

* **Separate Namespace:** Variables defined in the imported file reside in a separate namespace from the main playbook. This avoids accidental overrides or conflicts.

* **Static Import:** The tasks are imported at the beginning of the playbook execution, and any changes to the imported YAML file won't be reflected during the current run.

  

**Choosing Between Include and Import:**

  

* Use `include` when you have a small set of closely related tasks and want them to share the same variable namespace as the main playbook.

* Use `import` when you want to import reusable tasks with their own namespace to avoid conflicts. This is especially beneficial for modularizing playbooks and using the same tasks across multiple playbooks.

* If you need to dynamically update tasks based on changes in the included file during a single playbook run, consider using `include_tasks` instead of `include`. `include_tasks` re-reads the included file at the point of execution, reflecting any modifications.

  

Here's a table summarizing the key differences:

  

| Feature             | Include             | Import             |

|--------------------------|-------------------------|-------------------------|

| Task Insertion       | Inserts at current point | Imports at beginning |

| Variable Scope      | Shared namespace    | Separate namespace  |

| Update Reflection    | Reflected in current run | Not reflected (static)  |

| Namespace for Tasks | None                | Filename prefix     |

| Use Case             | Simple, related tasks   | Reusable, modular tasks |

  
  

Q: add cmd args

Ans: There are two main ways to pass variables at the command line when running Ansible playbooks:

  

**1. Using the `-e` option:**

  

The `-e` option (short for extra-vars) allows you to define key-value pairs for variables that you want to make available within your playbook. Here's the syntax:

  

```

ansible-playbook <playbook.yml> -e <variable_name>=<variable_value>

```

  

**Example:**

  

```

ansible-playbook install_apache.yml -e target_server=webserver1 package_name=httpd

```

  

In this example:

  

* `install_apache.yml` is the playbook you want to run.

* `target_server` is a variable set to "webserver1". This variable can be accessed within the playbook using Jinja2 templating syntax `{{ target_server }}`.

* `package_name` is another variable set to "httpd". This allows you to dynamically specify the package name within the playbook tasks.

  

**2. Using a Variable File:**

  

You can create a YAML file containing variables and pass it to the `ansible-playbook` command using the `-e` option along with the `@` symbol:

  

```yaml

\# variables.yml

target_server: webserver1

package_name: httpd

  

\# Running the playbook

ansible-playbook install_apache.yml -e @variables.yml

```

  

This approach helps keep your variables organized in a separate file and promotes code reusability.

  

**Additional Notes:**

  

* You can pass multiple variables at the same time using the `-e` option with multiple key-value pairs.

* Values passed at the command line using `-e` have higher precedence than variables defined within the playbook itself. This allows you to override default values defined in the playbook for specific runs.

* Be cautious with sensitive information like passwords when passing variables at the command line. Consider using alternative methods like environment variables or Ansible Vault for storing secrets.

  

Q: ask for prompts

Ans: Ansible provides the prompt module to request user input during playbook execution. This allows you to capture dynamic information from the user and use it within your tasks.

  

Q: why committing secrete data bad

Ans: Committing secret data like passwords, API keys, or encryption keys into a Git repository is a serious security risk for several reasons:

  

* **Version Control Exposure:**  Git is a version control system, meaning all revisions of your code, including past commits, are stored and accessible. Even if you remove the secret from a later commit, someone with access to the repository could potentially access it from an older version.

* **Accidental Leaks:** Accidental commits can happen due to human error. Pushing a commit with sensitive data before realizing it can expose your secrets.

* **Broader Repository Access:** Repositories might be shared with a wider group of people than you initially intended. Collaborators, testers, or even external parties might accidentally or intentionally gain access to sensitive information.

* **Compromised Credentials:** If an attacker gains access to your Git repository credentials, they have full access to all secrets stored within it.

  

Q: What are some of the ways to keep secret data out of a repository (Using Ansible)?

Ans: Ansible Vault is a built-in feature that allows you to encrypt sensitive information within your Ansible playbooks, variable files, or inventory files. This protects your secrets, such as passwords, API keys, or encryption keys, from being exposed in your version control system (VCS) like Git.

  

**Here's how Ansible Vault works:**

  

1. **Encryption:** You define a secret password for Ansible Vault. This password is used to encrypt the specific data you want to protect within your files. The encrypted data appears as gibberish within the files stored in your VCS repository, making it unreadable to anyone without the decryption password.

2. **Decryption on-the-fly:** During playbook execution, Ansible Vault utilizes the password you provided to decrypt the data on-demand for use within your tasks. This ensures that the sensitive information is only revealed when necessary and within the controlled environment of your Ansible automation.

  

**Benefits of Using Ansible Vault:**

  

* **Improved Security:** By encrypting secrets, you significantly reduce the risk of unauthorized access if your VCS repository is compromised.

* **Seamless Integration:** Ansible Vault is a built-in feature, so it integrates seamlessly with Ansible. There's minimal additional configuration required to start using it.

* **Ease of Use:** Ansible Vault offers a relatively simple way to encrypt and decrypt data within your playbooks.

  
  

**Drawbacks and Considerations:**

  

* **Single Password:** All your secrets are encrypted with the same password. If this password is compromised, all your secrets become vulnerable.

* **Password Management:** You need to manage and securely store the decryption password. A compromise of this password would negate the security benefits of Ansible Vault.

* **Limited Access Control:** Ansible Vault doesn't offer fine-grained access control for your secrets. Everyone with the decryption password has access to all encrypted data.

  
  

**When to Use Ansible Vault:**

  

* Ansible Vault is a good option for simple use cases where you have a few secrets to protect and don't require advanced access control features.

* It's a convenient way to get started with securing secrets within your Ansible playbooks.

  
  

**Alternatives for Stronger Security:**

  

For production environments with stricter security requirements, consider using external secrets management tools like HashiCorp Vault, AWS Secrets Manager, or Azure Key Vault. These tools offer features like:

  

* **Centralized Storage:** Secrets are stored outside your VCS repository, adding an extra layer of security.

* **Fine-grained Access Control:** You can define granular permissions for who can access and use specific secrets.

* **Auditing and Logging:** These tools provide detailed logs and audit trails to track access attempts and usage of secrets.

  

**Choosing the Right Approach:**

  

The choice between Ansible Vault and external secrets management tools depends on your specific security needs and the complexity of your environment.

  

* For simple use cases with a few secrets, Ansible Vault can be a good starting point.

* For production environments with stricter security requirements and a larger number of secrets, external secrets management tools offer a more robust and scalable solution.

  

Q: Where would you save Ansible configurations?

Ans: There are several common locations to store Ansible configurations, depending on their scope and purpose:

  

**1. Inventory File:**

  

* Typically stored in a central location accessible to all Ansible control nodes.

* Common locations include:

* `/etc/ansible/hosts` (default location on many Linux distributions)

* A shared directory accessible by all control nodes (e.g., `/shared/ansible/inventory`)

  

**2. Playbooks:**

  

* Playbooks can be stored in a central version control system (VCS) like Git for collaboration and version control.

* A common directory structure might be:

* `/playbooks` (generic location)

* Project-specific directories within `/playbooks` (e.g., `/playbooks/webservers`, `/playbooks/databases`)

  

**3. Roles:**

  

* Ansible roles, which are reusable collections of tasks and variables, are often stored in a VCS.

* Common locations include:

* A dedicated role repository like Ansible Galaxy

* A project-specific roles directory within your VCS (e.g., `/roles`)

  

**4. Variable Files:**

  

* Variable files, containing key-value pairs used within playbooks, can be stored in various locations:

* **Within Playbooks:** Define variables directly within your playbook YAML file.

* **Separate YAML Files:** Store variables in separate YAML files for better organization and reusability. These files can be located in the same directory as your playbooks or a centralized location.

* **Grouped by Environment:** If you have different environments (e.g., dev, test, prod), consider separate variable files for each environment to manage configuration differences.

  

**5. Ansible Configuration File (ansible.cfg):**

  

* The primary Ansible configuration file, `ansible.cfg`, is typically located in:

* `/etc/ansible/ansible.cfg` (default location)

* You can override this by setting the `ANSIBLE_CONFIG` environment variable or specifying a path in your command-line arguments.

  

**Choosing the Right Location:**

  

The ideal storage location depends on the specific configuration element and your overall infrastructure setup. Here are some general guidelines:

  

* **Centralized Access:** Inventory files and configuration files (`ansible.cfg`) should be accessible by all Ansible control nodes for consistent execution.

* **Version Control:** Use a VCS like Git to manage playbooks, roles, and reusable variable files for collaboration, version history, and rollback capabilities.

* **Separation of Concerns:** Separate sensitive information like passwords from playbooks and store them securely using Ansible Vault or external secrets management tools.

  

By following these practices, you can organize your Ansible configurations effectively, promoting maintainability, collaboration, and security within your automation workflows.

  

Q: where to read on ansible-modules

Ans: There are two primary places to look for documentation about an Ansible module:

  

1. **Ansible Project Documentation:**

  

   * The official Ansible documentation website provides comprehensive information about modules, plugins, concepts, and best practices.

   * The specific location for module documentation depends on the Ansible version you're using. Here's how to find it:

  

    * **Search the online documentation:**

        * Go to the Ansible documentation website: [https://docs.ansible.com/](https://docs.ansible.com/).

        * In the search bar, enter the name of the module you're interested in (e.g., "firewalld").

        * The search results should include the relevant module documentation page.

  

    * **Browse the documentation by version:**

        * Navigate to the Ansible documentation website.

        * Select the "Docs" tab and choose your Ansible version from the dropdown menu.

        * Within the version-specific documentation, explore sections like "Library Reference" or "Modules" to find the specific module you need.

  

2. **Module Documentation within Ansible:**

  

   * Ansible also provides a way to access module documentation directly from the command line. This can be helpful when you're already working in your Ansible environment.

   * Use the `ansible-module-docs` command followed by the module name:

  

    ```bash

    ansible-module-docs <module_name>

    ```

  

    * This command displays the module documentation directly in your terminal.

  

**Additional Resources:**

  

* **Ansible Galaxy:**

* If the module you're interested in comes from a community-developed role available on Ansible Galaxy, the role documentation might also provide specific information about the module's usage within that role.

  

**Choosing the Right Source:**

  

* The official Ansible documentation website is the most reliable and up-to-date source for module documentation.

* The `ansible-module-docs` command is a convenient way to access basic documentation within your Ansible environment.

* Consider checking Ansible Galaxy role documentation if the module belongs to a specific role you're using.

  

By using these resources effectively, you can find the information you need to understand and utilize Ansible modules for your automation tasks.

Q:install yum package in ansible

Ans: he yum module is the primary way to manage packages with the yum package manager in Ansible. Here's the basic syntax:

YAML

- name: Install package

  yum:

    name: <package_name>

    state: present  # Ensures the package is installed (default)

Q: How does Ansible indicate if something changed?

Ans: Ansible indicates if something changed during a task execution by returning a specific status. There are two main ways to determine if a change occurred:

  

**1. Task Status:**

  

Ansible playbooks are composed of individual tasks. Each task returns a status after execution, which can be:

  

* **changed:** This indicates that the task successfully made a change to the target system's configuration or state. This is the desired outcome for most configuration management tasks.

* **ok:** This status signifies that the task ran successfully, but no change was necessary. This might occur if the desired configuration already exists on the target system.

* **failed:** This status means the task encountered an error and failed to complete successfully. You'll need to investigate the error output for troubleshooting.

* **skipped:** This status indicates the task was skipped intentionally, either due to conditional logic within the playbook or missing requirements on the target system.

  

**2. Playbook Summary:**

  

After executing a playbook, Ansible displays a summary report. This report outlines the status (changed, ok, failed, skipped) for each task executed within the playbook. Here's what to look for:

  

* **CHANGED (X):** This signifies that at least one task within the playbook resulted in a change.

* **OK:** This indicates that all tasks ran successfully, but none of them made any changes (possibly because the desired configuration already existed).

* **FAILED (F):** This means at least one task failed during execution. You'll need to review the playbook output for specific error messages.

  

**Understanding the Difference:**

  

* **Task-Level Change:** The "changed" status at the task level indicates that a specific action within the task modified the target system.

* **Playbook-Level Change:** The "CHANGED (X)" in the playbook summary signifies that at least one task within the entire playbook resulted in a change.

  

Here are some additional points to consider:

  

* **Idempotence:** Ansible strives for idempotence, meaning a task should produce the same outcome (changed or ok) on subsequent runs if the target system's state hasn't changed manually.

* **Verbosity:**  You can control the level of detail displayed in the playbook summary using the `-v` or `--verbose` option with the `ansible-playbook` command. Higher verbosity levels provide more information about task execution and changes.

  

By understanding these concepts, you can effectively interpret Ansible's status outputs to determine if your playbooks are successfully making the desired changes to your managed systems.

  

Q: Where are some places variables can be registered?

Ans: In Ansible, variables can be registered during task execution using the `register` keyword. Here are the common places where you can register variables:

  

1. **Within Tasks:**

  

This is the most common scenario. You can register the output of any module within a task using the `register` keyword. The output is then stored in a variable with a user-defined name for later use:

  

```yaml

- name: Get system uptime

  debug:

msg: "System uptime: {{ uptime }} seconds"

  register: uptime_info

```

  

In this example:

  

* The `debug` module displays the system uptime.

* The `register: uptime_info` part captures the output (uptime in seconds) and stores it in a variable named `uptime_info`.

  

2. **Inside Loops:**

  

When using loops in Ansible playbooks, you can register the results for each iteration:

  

```yaml

- name: Manage user accounts

  user:

name: "{{ item.username }}"

state: "{{ item.state }}"

  loop: "{{ users }}"

  register: user_results

```

  

Here:

  

* The loop iterates over a list of user definitions (`users`).

* The `register: user_results` captures the results (success/failure) for each user creation/deletion in a variable named `user_results`.

  

3. **Within Blocks:**

  

Blocks allow you to group tasks with conditional logic. You can register the outcome of the entire block execution:

  

```yaml

- name: Reboot if uptime exceeds threshold

  block:

when: uptime | float >= uptime_threshold

tasks:

   - name: Reboot the server

     reboot:

  register: reboot_action

```

  

* This block checks the uptime and reboots only if it exceeds the threshold.

* `register: reboot_action` captures whether a reboot occurred (changed) or not (ok).

  

4. **For Handlers:**

  

Handlers are tasks that execute only when a specific condition is met. You can register information about the triggering event within the handler:

  

```yaml

- name: Notify admins on reboot

  mail:

to: admins@example.com

subject: "Server {{ ansible_hostname }} Rebooted"

  when: changed  # Run only if a reboot occurred (from previous task)

  register: mail_sent

```

  

* This handler sends an email notification on reboot.

* `register: mail_sent` captures whether the email was sent (changed) or not (failed).

  

By understanding these locations for registering variables, you can effectively capture and utilize the output of tasks and blocks within your Ansible playbooks for conditional execution, data manipulation, and reporting purposes.

  

Q: How are variables referenced?

Ans: In Ansible, variables are referenced within playbooks, tasks, and templates using a specific syntax. Here's how you can use variables effectively:

  

**1. Double Curly Braces (Jinja2 Templating):**

  

The primary method for referencing variables is through double curly braces `{{ }}`. This syntax leverages Jinja2 templating, which allows you to embed variables within strings and data structures.

  

```yaml

- name: Print system information

  debug:

msg: "Hostname: {{ ansible_hostname }}, Uptime: {{ uptime }} seconds"

```

  

In this example:

  

* `ansible_hostname` is a fact (predefined variable) containing the hostname of the managed system.

* `uptime` is likely a variable registered in a previous task to store the system uptime.

  

**2. Accessing Dictionary Values:**

  

You can access specific values within dictionaries using dot notation:

  

```yaml

- name: Configure service

  systemd:

name: my_service

state: started

enabled: yes

daemon_reload: yes

  vars:

service_config:

   Restart: "on-failure"

   User: "myuser"

```

  

Here:

  

* The `service_config` dictionary defines service configuration options.

* Within the `systemd` module, individual options like `Restart` and `User` reference their corresponding values from the dictionary using dot notation (`service_config.Restart`, `service_config.User`).

  

**3. Loops and Conditionals:**

  

Variables can be used within loops and conditional statements to control playbook execution:

  

```yaml

- name: Manage users

  user:

name: "{{ item.username }}"

state: "{{ item.state }}"

  loop: "{{ users }}"

  when: item.state == "present"  # Only create users if state is "present"

```

  

* This example iterates through a list of users (`users`) defined as a variable.

* The loop references individual user information within the `user` module using `item.username` and `item.state`.

* The `when` condition ensures user creation only occurs if the `state` is set to "present".

  

**4. Filters (Optional):**

  

Jinja2 offers various filters you can apply to manipulate variable values within templates:

  

```yaml

- name: Display formatted uptime

  debug:

msg: "Uptime: {{ uptime | float | round(2) }} seconds"

```

  

* This example uses filters:

* `float`: Converts uptime to a floating-point number.

* `round(2)`: Rounds the uptime value to two decimal places.

  

**5. Referencing Facts:**

  

Ansible provides predefined variables called facts, which contain information about the managed systems. These facts can be directly referenced using the `ansible_` prefix:

  

```yaml

- name: Check if package is installed

  debug:

msg: "Package httpd is installed: {{ ansible_pkgs.contains('httpd') }}"

```

  

* This example checks if the `httpd` package is installed using the `ansible_pkgs.contains` fact.

  

**Remember:**

  

* Ensure the variable you're referencing is defined or registered before attempting to use it.

* Use appropriate quoting (single or double quotes) when referencing variables within strings.

  

By following these guidelines, you can effectively reference variables to create dynamic and data-driven playbooks in Ansible.

  

Q: How do you loop over a list or a dictionary in Ansible?

Ans:Ansible offers two primary ways to loop over a list or dictionary within your playbooks:

  

**1. Looping over a List:**

  

The `with_items` keyword is the most common method for iterating over a list of items in Ansible. Here's the syntax:

  

```yaml

- name: Loop over a list of users

  user:

name: "{{ item }}"

state: present

  with_items: "{{ users }}"

```

  

**Explanation:**

  

* `- name`: Defines the task name.

* `user`: Specifies the module you want to execute for each item in the loop (creates users in this example).

* `with_items`: This keyword initiates the loop.

* `{{ users }}`: This references the variable containing the list of items (usernames in this case).

* `{{ item }}`: Inside the loop, `item` represents the current item during each iteration. You can access its value within the module arguments.

  

**2. Looping over a Dictionary:**

  

To iterate over key-value pairs within a dictionary, use the `loop_control` keyword:

  

```yaml

- name: Configure services

  systemd:

name: "{{ item.key }}"

state: started

enabled: yes

  loop_control:

loop_var: item

  loop: "{{ services }}"

```

  

**Explanation:**

  

* `loop_control`: This keyword initiates the loop for dictionaries.

* `loop_var: item`: This defines the variable name (`item` in this example) that represents each key-value pair during iteration.

* `loop: "{{ services }}`: This references the dictionary variable containing service configurations (key: service name, value: configuration options).

* Inside the loop, you can access the key using `item.key` and the value using `item.value` (depending on your dictionary structure).

  

**Additional Considerations:**

  

* **Loop Filters (Optional):** You can use Jinja2 filters within loops to manipulate items before using them in modules.

* **Conditionals within Loops:** You can combine loops with conditional statements (`when`) to control task execution based on specific item properties.

* **Looping over Nested Structures:** Ansible allows looping over nested lists or dictionaries within loops for more complex scenarios.

  

Understanding these methods allows you to automate repetitive tasks for multiple systems or resources based on the data provided in your lists or dictionaries.

  

By effectively using loops in your Ansible playbooks, you can achieve efficient and scalable automation for managing your infrastructure.