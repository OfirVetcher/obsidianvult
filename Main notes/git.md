
2024-06-19 19:05
Status: #done
Tags: [[Git]]

# git


## Understanding Git Concepts

**What is Git?**

Git is a distributed version control system (DVCS) that allows you to track changes in your code or files over time. It provides a way to:

- Maintain a history of changes.

- Collaborate with others on projects.

- Revert to previous versions of your code.

- Efficiently manage different versions (branches) of your project.

  

**Key Concepts:**

  

1. **Commit:**

   - A commit is a snapshot of your project at a specific point in time. It includes the modified files, the commit message you provide, and a reference to the previous commit.

   - Commits create a chronological history of your project's evolution.

  

2. **Branch:**

   - Branches are independent lines of development within a Git repository. They allow you to work on new features or bug fixes without affecting the main codebase (master branch by default).

   - You can create, switch between, and merge branches to integrate your changes.

  

3. **HEAD:**

   - HEAD is a reference that points to the latest commit in your current working directory. It essentially indicates the state of your project at that specific point.

  

4. **Local vs. Remote:**

   - **Local Repository:** The copy of your project on your own machine, containing all the files, commits, and branches.

   - **Remote Repository:** A copy of your project hosted on a server (e.g., GitHub, GitLab) that allows collaboration and sharing with others.

  

**Creating a New Branch:**

```bash

git branch <branch_name> # Create a new branch named "<branch_name>"

```

  

**Moving Between Branches:**

```bash

git checkout <branch_name>   # Switch to the branch named "<branch_name>"

```

**Edited Files and Branch Switching:**

- Changes made in one branch are not reflected in another until you merge them.

- When switching branches, files that differ between branches might be marked as "modified" or "conflicted" (if edits clash).

  

**Git Stash:**

- Temporarily stores your current uncommitted changes for later use.

- Useful if you need to switch tasks or branches without losing your ongoing work.

  

**Git Reset:**

- Allows you to move the HEAD pointer to a previous commit, essentially rewinding your project's history.

- Be cautious as this can rewrite history and potentially lose uncommitted changes.

  

**Git Merge:**

- Combines the changes from one branch into another.

- If there are conflicts (overlapping edits), you'll need to manually resolve them before a successful merge.

  

**Creating a Conflict Scenario:**

1. Create a new branch named `feature`.

2. Edit a file in both the `master` and `feature` branches and commit the changes in each branch with different content.

3. Switch to `master` and try to merge `feature`. This will result in a conflict that you need to resolve manually.

  

**Git Rebase:**

- Re-writes your branch history by replaying its commits on top of another branch (usually `master`).

- Can be useful for keeping your branches clean and linear, but use it with caution as it rewrites history.


**GitLab Repository:**

1. Create a username (if you don't have one) on GitLab ([https://about.gitlab.com/](https://about.gitlab.com/)).

2. Create a new repository on GitLab to store your Ansible scripts.

3. Follow the instructions provided by GitLab to connect your local Git repository to the remote repository on GitLab.
  

This provides a basic understanding of Git. There are many resources available online for further learning and exploring advanced features. Remember, practice and experimentation are key to mastering Git!
References: