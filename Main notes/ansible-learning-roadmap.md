
2024-06-30 17:34
Status: #inProgress 
Tags: [[DevOps]] [[Ansible]] [[RedHat]]

# ansible-learning-roadmap

This lesson plan provides a structured approach to learning Ansible, incorporating theory and practical exercises for better retention. Each lesson includes estimated time for completion and focuses on building your skills progressively.

**Week 1: Fundamentals**

- [x] **Lesson 1: Introduction to Ansible (2 hours)**

- **Objectives:**
    - Understand the core concepts of infrastructure automation.
    - Identify the benefits and use cases of Ansible.
- **Exercises:**
    - [x] **Activity 1:** Write a short paragraph explaining what infrastructure automation is and its advantages in IT environments.
    - [x] **Activity 2:** Research and list three real-world scenarios where Ansible can be used for automation.

- [x] **Lesson 2: Setting Up the Environment (3 hours)**

- **Objectives:**
    - Install and configure Ansible on a control machine.
    - Set up SSH key-based authentication for managing remote systems.
-  **Exercises:**
    - [x] **Activity 1:** Following the official documentation, install Ansible on your control machine (choose a suitable method based on your OS).
    - [x] **Activity 2:** Set up SSH key-based authentication between your control machine and two target nodes you'll use for practice. Refer to the documentation for guidance [https://linux.die.net/man/1/ssh-keygen](https://linux.die.net/man/1/ssh-keygen).

- [ ] **Lesson 3: Inventories and Playbooks (4 hours)**

- **Objectives:**
    - Define inventories to manage groups of manageable systems.
    -  Create basic playbooks to automate tasks.
- **Exercises:**
    - [ ] **Activity 1:** Create a static inventory file listing your two target nodes and categorize them into a group named 'webservers'.
    - [ ] **Activity 2:** Write a simple playbook named "install_apache.yml" that uses the 'yum' module to install the Apache web server package on all webservers in your inventory.
    - [ ] **Activity 3:** Run the playbook and verify the Apache installation on both target nodes.

**Week 2: Deepening Your Skills**

- [ ] **Lesson 4: Modules and Ansible Galaxy (3 hours)**

- **Objectives:**
    - Explore the concept of Ansible modules and their role in automation tasks.
    - Discover Ansible Galaxy as a repository for reusable content.
- **Exercises:**
    - [ ] **Activity 1:** Browse the Ansible module documentation [invalid URL removed] and explore different modules available. Choose two modules that interest you and write a brief description of their functionalities.
    - [ ] **Activity 2:** Visit Ansible Galaxy [[https://galaxy.ansible.com/](https://galaxy.ansible.com/)] and search for a role that automates user creation on Linux systems. Install and explore the role to understand how it's structured.

- [ ] **Lesson 5: Variables and Facts (2 hours)**

- **Objectives:**
    - Utilize variables to store and manage reusable data in playbooks.
    - Leverage facts to gather information from managed nodes.
- **Exercises:**
    - [ ] **Activity 1:** Modify your "install_apache.yml" playbook to use a variable for the Apache package name. This allows you to easily switch between httpd and apache2 packages if needed.
    - [ ] **Activity 2:** Write a new playbook that uses the 'setup' module to gather facts about the installed kernel version on your webservers. Display the gathered facts within the playbook.

**Lesson 6: Conditional Logic and Loops (4 hours)**

- **Objectives:**
    - Implement conditional statements (if/else) to handle different scenarios in playbooks.
    - Utilize loops to automate tasks that need to be repeated across multiple systems.
- **Exercises:**
    - [ ] **Activity 1:** Extend the "install_apache.yml" playbook to check if the Apache package is already installed using the 'when' condition. If not installed, proceed with the installation.
    - [ ] **Activity 2:** Write a playbook that iterates through a list of web applications (defined as a variable) and creates directories and files with specific permissions for each application on your webservers.

**Week 3: Putting it Together (Ongoing)**

**Lesson 7: Advanced Playbook Techniques (2 hours**

- **Objectives:**
    - Explore advanced playbook features like includes, handlers, and error handling.
- **Exercises:**
    - [ ] **Activity 1:** Refactor your previous playbooks to utilize includes for reusable tasks like stopping and starting Apache.
References: