
2024-06-30 18:22
Status: #inProgress 
Tags: [[DevOps]] [[Ansible]] [[RedHat]] [[ansible-learning-roadmap]]

# ansible-lesson-3
## ansible inventories
### what is an inventory
an inventory is a file or a group of files that defines the managed hosts and groups of hosts ansible can automate tasks on
in ansible there are 2 ways to define your inventory one is with yml syntax and the other is with the ini syntax 
#### Yml inventory
Yml(Yaml Ain't Markup language) is a human readable data serialization format widely used in ansible inventories here is a basic syntax:
``` YAML
# Define individual hosts
server1:
  hosts: 192.168.1.10

server2:
  hosts: 192.168.1.11

# Define groups of hosts
webservers:
  hosts:
    - server1
    - server2

# Define variables for specific hosts or groups
database:
  hosts: 10.0.0.10
  vars:
    db_name: mydatabase
```
- Key-value pairs: inventory entries use Key-value pairs. The key is typically the host name or group name
- hosts: the hosts key specifies the ip address, hostname, or other unique identifier of a managed system. you can define multiple hosts under a group using indentation
- Groups: Groups are defined with a colon (:) following the group name. The hosts key under a group lists the member hosts of that group- Variables: You can define variables under individual hosts or groups using the vars key. These variables hold data specific to that host or group and can be used within playbooks

#### INI inventory:
INI (initialization file) format is another option for ansible inventories while less common then Yaml, it offers a simpler structure here's the syntax: 
``` Ini, TOML
[server1]
host = 192.168.1.10

[server2]
host = 192.168.1.11

[webservers]
server1
server2

[database]
host = 10.0.0.10
db_name = mydatabase

```
- sections: inventories are devided into sections, typically named after individual hosts or Groups
- host: The host key specifies the ip address or hostname of a managed system 
- group membership: You can lit hostnames directly under a group section for group membership
- variables: variable are defined directly within a section using a Key-value pair format
#### additional points:
- comments: Both Yaml and ini formats allow comments using ```#``` at the beginning of a line
- whitespace: Yaml relies on indentation for structure while whitespace is less critical in ini
- other inventory formats: Ansible supports other inventory formats like json and dynamic inventories through plugins

References:
