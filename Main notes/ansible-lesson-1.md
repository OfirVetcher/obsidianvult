
2024-06-30 17:57
Status: #done  
Tags: [[DevOps]] [[Ansible]] [[RedHat]] [[ansible-learning-roadmap]]

# ansible-lesson-1
what is infrastructure automation ?
infrastructure automation is the use of technology that  performs tasks with reduced human assistance in order to control the hardware, software, networking, components, operating systems, and data storage components used to deliver information technology, services and solutions

what are the use cases for ansible ?
Server provisioning  and configuration:
 - Bare-metal server setup : automate the entire process of provisioning  bare-metal servers , including os installation, configuration management and software installation 
 - disaster recovery: develop playbooks to automate disaster recovery process including deploying new servers, restoring data backups and reconfiguring applications
 applications deployment and management:
 - database management : use ansible to automate database creation , user management, and schema deployments ensuring consistent database environments 
References: